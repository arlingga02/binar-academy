const {Client} = require('pg')
let client = new Client({
    user: 'postgres',
    host: 'localhost',
    password: '021201',
    port: 5432,
})

//creating user and password for login
const userLog = {
    email: "lingga@gmail.com",
    password: "021201",
}

//creating function for authentication user
function auth(userLog){
    return ("lingga@gmail.com" === userLog.email) && ("021201" === userLog.password);
}

//creating database
function createDatabase(databaseName){
    if (auth(userLog)){
        const queryCreateDatabase = `CREATE DATABASE ${databaseName};`;
        client.connect()
        .then(() => console.log(`Connected for create database ${databaseName}`))
        .then(() => client.query(queryCreateDatabase))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
    else{
        client.end()
        console.log("You don't have access!")
    }
}

//connecting to database that already build before
function useDB() {
    client = new Client({
        user: 'postgres',
        host: 'localhost',
        password: '021201',
        port: 5432,
        database: 'db_user_game',
    });
}

//creating for table database
function createTable(){
        if(auth(userLog)){
            useDB();
            const queryCreateTableUserGame = `CREATE TABLE IF NOT EXISTS user_game (
                user_id BIGSERIAL NOT NULL PRIMARY KEY,
                email VARCHAR(50) NOT NULL,
                password VARCHAR(50) NOT NULL,
                user_bio_id BIGSERIAL NOT NULL,
                user_game_hist_id BIGSERIAL NOT NULL);`;
            const queryCreateTableUserBio = `CREATE TABLE IF NOT EXISTS user_bio (
                user_bio_id BIGSERIAL NOT NULL PRIMARY KEY,
                user_name VARCHAR(50) NOT NULL,
                user_age INTEGER NOT NULL,
                gender VARCHAR(1) NOT NULL);`;
            const queryCreateTableHist = `CREATE TABLE IF NOT EXISTS user_game_history (
                user_game_hist_id BIGSERIAL NOT NULL PRIMARY KEY,
                much_game INTEGER NOT NULL,
                much_win INTEGER NOT NULL,
                much_lose INTEGER NOT NULL);`;

            client.connect()
            .then(() => client.query(queryCreateTableUserGame))
            .then(() => client.query(queryCreateTableUserBio))
            .then(() => client.query(queryCreateTableHist))
            .then(() => console.log("Can make the table"))
            .then(() => client.end())
            .catch(err => console.error(err))
    }
}

//create relation
function createRelation(){
    if(auth(userLog)){
        useDB();
        const queryRelationUserBio = `ALTER TABLE user_game 
            ADD UNIQUE CONSTRAINT user_bio_id_fk FOREIGN KEY (user_bio_id) 
            REFERENCES user_bio(user_bio_id);`;
        
        const queryRelationUserHist = `ALTER TABLE user_game
            ADD CONSTRAINT user_game_hist_id_fk FOREIGN KEY (user_game_hist_id) 
            REFERENCES user_game_history(user_game_hist_id);`;

        client.connect()
        .then(() => client.query(queryRelationUserBio))
        .then(() => client.query(queryRelationUserHist))
        .then(() => console.log("Can make the relation"))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

//inserting data
function insertDataUserBio(){
    if(auth(userLog)){
        useDB();
        const plusDataUserBio = `INSERT INTO user_bio (user_name, user_age, gender) VALUES ('pulse', 21, 'L');`;
        
        client.connect()
        .then(() => client.query(plusDataUserBio))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_bio"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

function insertDataUserGame(){
    if(auth(userLog)){
        useDB();
        const plusDataUserGame = `INSERT INTO user_game (email, password, user_bio_id, user_game_hist_id) 
        VALUES ('arlingga@mail.com', '021201', 1, 1);`;
        
        client.connect()
        .then(() => client.query(plusDataUserGame))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_game"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}


function insertDataUserHist(){
    if(auth(userLog)){
        useDB();
        const plusDataUserHistory = `INSERT INTO user_game_history (much_game, much_win, much_lose)
        VALUES (50, 30, 20);`;
        
        client.connect()
        .then(() => client.query(plusDataUserHistory))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_game_history"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

//just read data
function readDataUserBio() {
    if(auth(userLog)){
        useDB();
        client.connect()
        .then(() => client.query("select * from user_bio"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

function readDataUserGame() {
    if(auth(userLog)){
        useDB();
        client.connect()
        .then(() => client.query("select * from user_game"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

function readDataUserHist(){
    if(auth(userLog)){
        useDB();
        client.connect()
        .then(() => client.query("select * from user_game_history"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

//read data after join
function joinData1() {
    if(auth(userLog)){
        useDB();
        client.connect()
        .then(() => client.query(`SELECT email ,user_bio.user_name ,user_bio.user_age FROM user_game
                                    INNER JOIN user_bio
                                        ON user_game.user_bio_id = user_bio.user_bio_id;`))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

function joinData2(){
    if(auth(userLog)){
        useDB();
        client.connect()
        .then(() => client.query(`SELECT email ,user_bio.user_name ,user_game_history.much_game ,user_game_history.much_win, 
                            user_game_history.much_lose FROM user_game
                                INNER JOIN user_game_history 
                                    ON user_game.user_game_hist_id = user_game_history.user_game_hist_id
                                INNER JOIN user_bio 
                                    ON user_game.user_bio_id = user_bio.user_bio_id;`))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

//update data after insert
function updateDataUserHist(id){
    if(auth(userLog)){
        useDB();
        const upDataUserHistory = `UPDATE user_game_history 
        SET 
        much_game = 70, 
        much_win = 50,
        much_lose = 20 
        WHERE user_game_hist_id = ${id};`;
        
        client.connect()
        .then(() => client.query(upDataUserHistory))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_game_history"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

function updateDataUserbio(id){
    if(auth(userLog)){
        useDB();
        const upDataUserBio = `UPDATE user_bio 
        SET 
        user_name = 'Jimin', 
        user_age = 22
        WHERE user_bio_id = ${id};`;
        
        client.connect()
        .then(() => client.query(upDataUserBio))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_bio"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

function updateDataGame(id){
    if(auth(userLog)){
        useDB();
        const upDataUserGame = `UPDATE user_game 
        SET 
        email = 'ji@mail.com', 
        password = 5595
        WHERE user_id = ${id};`;
        
        client.connect()
        .then(() => client.query(upDataUserGame))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_game"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

//delete data after successful every action
function delDataUserBio(id){
    if(auth(userLog)){
        useDB();
        const delDataUserBio = `DELETE FROM user_bio 
        WHERE user_bio_id = ${id};`;
        
        client.connect()
        .then(() => client.query(delDataUserBio))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_bio"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

function delDataUserHistory(id) {
    if(auth(userLog)){
        useDB();
        const delDataUserHistory = `DELETE FROM user_game_history 
        WHERE user_game_hist_id = ${id};`;
        
        client.connect()
        .then(() => client.query(delDataUserHistory))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_game_history"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

function delDataUserGame(id){
    if(auth(userLog)){
        useDB();
        const delDataUserGame = `DELETE FROM user_game 
        WHERE user_id = ${id};`;
        
        client.connect()
        .then(() => client.query(delDataUserGame))
        .then(() => console.log("Can make the data"))
        .then(() => client.query("select * from user_game"))
        .then(result => console.table(result.rows))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
}

//instruksi

    // Pertama tama ini harus mengikuti instruksi, dari nomor 1 sampai 3
    //1. Creating Database, dan ketika ingin 
    // use db, harus lihat pada line 50, serta ganti nama database yang diinputkan,
    // ke object key database: 'nama-database-yang-diinput'
// createDatabase("db_user_game");
    //2. Making Table
// createTable();
    //3. Making Relation
// createRelation();
    //After 3 steps above, bisa melakukan operasi dibawah ini dengan insert data dahulu.

//INSERT DATA
    //4. Insert data, ketika ingin insert data tolong ubah nama yang ada di line 108, didalam value, untuk user bio
// insertDataUserBio(); 
    //Insert data, untuk user_game, insert data tolong ubah isian values pada line 124, untuk user game
// insertDataUserGame();
    //Insert data, untuk user_game_history, insert data tolong ubah isian values pada line 141, untuk data user game history
// insertDataUserHist();

//READ DATA
    //Read data user bio
// readDataUserBio();
    //Read data user game
// readDataUserGame();
    //Read data user game history
// readDataUserHist();

    // JOIN to see email, username, user_age
//joinData1();
    //Join to see email, username, much_game, much_win, much_lose
//  joinData2();

//UPDATE DATABASE
    //each update have to be edited on the line of each function
    //update data user game history
// updateDataUserHist(1);
    //update data user bio
// updateDataUserbio(1)
    //update data user game
// updateDataGame(1);

//DELETE DATA
// delDataUserBio(1);
// delDataUserHistory(1);
// delDataUserGame(1);