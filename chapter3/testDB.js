const {Client} = require('pg')
const client = new Client({
    user: 'postgres',
    host: 'localhost',
    password: '021201',
    port: 5432,
})

// //inserting to table
// client.connect()
// .then(() => console.log("Connected fo sho"))
// .then(() => client.query("insert into person values($1, $2, $3)", [4, "Rudy", "UK"]))
// .then(() => client.query("select * from person"))
// .then(result => console.table(result.rows))
// .catch(err => console.error(err))
// .finally(() => client.end())

//creating user and password for login
const userLog = {
    email: "lingga",
    password: "021201",
}

//creating function for authentication user
function auth(userLog){
    return ("lingga" === userLog.email) && ("021201" === userLog.password);
}

function createDatabase(databaseName){
    if (auth(userLog)){
        const queryCreateDatabase = `CREATE DATABASE ${databaseName};`;
        client.connect()
        .then(() => console.log(`Connected for create database ${databaseName}`))
        .then(() => client.query(queryCreateDatabase))
        .then(() => client.query(queryCreateTable))
        .then(() => client.end())
        .catch(err => console.error(err))
    }
    else{
        client.end()
        console.log("You don't have access!")
    }
}

function useDB() {
    const client = new Client({
        user: 'postgres',
        host: 'localhost',
        password: '021201',
        port: 5432,
        database: databaseName,
    });
    return client;
}

//creating for table database
function createTable(){
        if(auth(userLog)){
            const client = useDB();
            const queryCreateTableUserGame = `
            CREATE TABLE IF NOT EXISTS user_game (
                user_id BIGSERIAL NOT NULL PRIMARY KEY,
                email VARCHAR(50) NOT NULL,
                password VARCHAR(50) NOT NULL,
                user_bio_id BIGSERIAL NOT NULL,
                user_game_hist_id BIGSERIAL NOT NULL,
            );
            `;
            const queryCreateTableUserBio = `
            CREATE TABLE IF NOT EXISTS user_bio (
                user_bio_id BIGSERIAL NOT NULL PRIMARY KEY,
                user_name VARCHAR(50) NOT NULL,
                user_age INTEGER NOT NULL,
                gender VARCHAR(1) NOT NULL,
            );
            `;
            const queryCreateTableHist = `
            CREATE TABLE IF NOT EXISTS user_game_history (
                user_game_hist_id BIGSERIAL NOT NULL PRIMARY KEY,
                much_game INTEGER NOT NULL,
                much_win INTEGER NOT NULL,
                much_lose INTEGER NOT NULL,
            );
            `;
                
            client.connect()
            .then(() => client)
            .then(() => client.query(queryCreateTableUserGame))
            .then(() => client.query(queryCreateTableUserBio))
            .then(() => client.query(queryCreateTableHist))
            .then(() => console.log("Can make the table"))
            .then(() => client.end())
            .catch(err => console.error(err))
    }
}

//1. Creating Database 
createDatabase("data_user_game");
//2. Making Table and relation
createTable();


//3. Read data in all tables
//4. Read data in join tables
//5. Insert data in table
//6. Update data in table
//7. Delete data in table
