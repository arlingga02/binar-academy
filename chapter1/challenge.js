rl.on('close', function () {const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Arlingga Cahya Ramdhana
// BE-JS1

function sum() {
  console.log("=====================================================================================");
  rl.question('What is your first number ? ', function (sum1) {
    rl.question('What is your second number ? ', function (sum2) {
      let resultSum = (+sum1) + (+sum2)
      console.log(`${sum1} + ${sum2} = ${resultSum}`);
      repeat();
    });
  });
}

function minus() {
  console.log("=====================================================================================");
  rl.question('What is your number 1 ? ', function (min1) {
    rl.question('What is your number 2 ? ', function (min2) {
      let resultMinus = (+min1) - (+min2)
      console.log(`${min1} - ${min2} = ${resultMinus}`);
      repeat();
    });
  });
}

function multiply() {
  console.log("=====================================================================================");
  rl.question('What is your number 1 ? ', function (multi1) {
    rl.question('What is your number 2 ? ', function (multi2) {
      let resultMultiply = (+multi1) * (+multi2)
      console.log(`${multi1} x ${multi2} = ${resultMultiply}`);
      repeat();
    });
  });
}

function divide() {
  console.log("=====================================================================================");
  rl.question('What is your number 1 ? ', function (div1) {
    rl.question('What is your number 2 ? ', function (div2) {
      let resultDivide = Math.floor(+div1) / Math.floor(+div22)
      console.log(`${div1} : ${div2} = ${Math.round(resultDivide)}`);
      repeat();
    });
  });
}

function squared() {
  console.log("=====================================================================================");
  rl.question('What is your number for being squared ? ', function (squared1) {
    rl.question('What is your number for the squared ? ', function (squared2) {
      let resultSquared = (+squared1) ** (+squared2)
      console.log(`${squared1} ^${squared2} = ${resultSquared}`);
      repeat();
    });
  });
}

function squareArea() {
  console.log("=====================================================================================");
  rl.question('What is your number of side in square ? ', function (side) {
    let resultSquareArea = (+side) ** 2;
    console.log(`Result Square Area = ${resultSquareArea}`);
    repeat();
  });
}

function volumeCube() {
  console.log("=====================================================================================");
  rl.question('What is your number of cm for side of cube ? ', function (sideCube) {
    let resultSquareArea = (+sideCube) ** 3;
    console.log(`Result volume Cube = ${resultSquareArea}`);
    repeat();
  });
}

function volumeTube() {
  console.log("=====================================================================================");
  rl.question('What is your radius ? ', function (radius) {
    rl.question('What is your height ? ', function (height) {
      let resultVolumeTube = 3.14 * +radius * +radius * +height;
      console.log(`Result Volume Tube = ${resultVolumeTube}`);
      repeat();
    });
  });
}

function decide() {
  console.log(`This is for calculate Sum, Minus, Multiply, Divide, Squared, Area Square, Volume Cube and Tube
  =====================================================================================
  Now u have to choose (the Number) between that, here is the list;
  1. Sum.   2. Minus.   3. Multiply   4. Divide. 5. Squared
  6. Area of Square,    7. Volume of Cube.       8. Volume of Tube
  `);
  rl.question('Choose ? ', function (decide) {
    switch (decide) {
      case '1':
        sum();
        break;
      case '2':
        minus();
        break;
      case '3':
        multiply();
        break;
      case '4':
        divide();
        break;
      case '5':
        squared();
        break;
      case '6':
        squareArea();
        break;
      case '7':
        volumeCube();
        break;
      case '8':
        volumeTube();
        break;
      default:
        break;
    }
  });
}

function repeat() {
  rl.question('You want to repeat ? ', function (rep) {
    rep = rep.toLowerCase()
    if (rep === 'yes') {
      decide();
    } else if (rep === 'no') {
      rl.close();
    } else {
      return `error`;
    }
  });
}

decide();

rl.on('close', function () {
  console.log('\nThank You !!!');
  process.exit(0);
});
  console.log('\nIM DONE !!!');
  process.exit(0);
});
