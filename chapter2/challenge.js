"use strict";

const ps = require('prompt-sync');
const prompt = ps()

function core(num) {
    let student = prompt("Banyak Mahasiswa yang ingin dihitung? ")
    console.log('=================================================');
    //iteration
    let mhsTidakLulus = 0;
    let mhsLulus = 0;
    let arr = [];

    // input nilai sekaligus seleksi murid yang lulus dan tidak, dan
    // menghitung ada berapa murid yang lulus dan tidak
    for (let i = 1; i <= student; i++) {
        num = prompt(`Masukkan nilai mahasiswa ke ${i}: `)
        num = parseInt(num)
        if (num < 75) {
            mhsTidakLulus++;
        } else if (num => 75 && num <= 100) {
            mhsLulus++;
        }
        //push num to arr
        arr.push(num);

    }
    // sorting data input
    arr.sort((a, b) => { return a - b });

    // find average
    let sum = 0;
    let count = 0;
    arr.forEach((item) => {
        sum += item;
        count++;
    });
    let avg = sum / count;


    // would be an output of everything that has to be
    console.log('============================================================================================');
    console.log('Result: ');
    console.log(`Jumlah Mahasiswa yang lulus: ${mhsLulus}, Jumlah Mahasiswa yang tidak lulus: ${mhsTidakLulus}`);
    console.log(`Nilai tertinggi: ${Math.max(...arr)}`)
    console.log(`Nilai terendah: ${Math.min(...arr)}`)
    console.log(`Urutan nilai Mahasiswa dari terendah ke tinggi: ${arr}`)
    console.log(`Rata rata nilai dari mahasiswa: ${avg.toFixed(1)}`)


}


core();